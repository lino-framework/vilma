==========================
The ``lino-vilma`` package
==========================




Lino Vilma is a customizable contact management system for villages.

- Developer specs:
  https://www.lino-framework.org/specs/vilma

- For *introductions* and *commercial information* about Lino Vilma
  please see `www.saffre-rumma.net
  <https://www.saffre-rumma.net/noi/>`__.



